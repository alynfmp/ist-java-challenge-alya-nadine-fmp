package ist.challenge.Alya_nadine_fmp.DTO;

import ist.challenge.Alya_nadine_fmp.entity.User;
import lombok.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrationDTO {

    @Size(max = 50)
    private String username;

    @Size(min = 6, max=40)
    private String password;

    public User convertToEntity(){
        return User.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}
