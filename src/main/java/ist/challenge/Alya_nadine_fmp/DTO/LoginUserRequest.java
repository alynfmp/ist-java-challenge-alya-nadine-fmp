package ist.challenge.Alya_nadine_fmp.DTO;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
public class LoginUserRequest {
    
    private String username;

    private String password;
}
