package ist.challenge.Alya_nadine_fmp.DTO;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponseDTO {

    private Long user_id;

    private String username;

    private List<String> roles;

    @Override
    public String toString() {
        return "UserResponseDTO{" +
                "user_id=" + user_id +
                ", username='" + username + '\'' +
                ", roles=" + roles +
                '}';
    }
}
