package ist.challenge.Alya_nadine_fmp.service;


import ist.challenge.Alya_nadine_fmp.DTO.LoginUserRequest;
import ist.challenge.Alya_nadine_fmp.DTO.RegistrationDTO;
import ist.challenge.Alya_nadine_fmp.DTO.UpdateUserDTO;
import ist.challenge.Alya_nadine_fmp.entity.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface UserService {

    ResponseEntity<Object> authenticationUser(LoginUserRequest userRequest) throws Exception;
    ResponseEntity<Object> registrationUser(RegistrationDTO user) throws Exception;
    ResponseEntity<Object> findAllUser()throws Exception;
    ResponseEntity<Object> findMyUser(String keyword) throws Exception;
    ResponseEntity<Object> updateUser(UpdateUserDTO user, Authentication authentication) throws Exception;
}
