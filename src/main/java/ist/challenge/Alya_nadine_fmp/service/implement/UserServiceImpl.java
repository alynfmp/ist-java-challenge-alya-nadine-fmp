package ist.challenge.Alya_nadine_fmp.service.implement;

import ist.challenge.Alya_nadine_fmp.DTO.*;
import ist.challenge.Alya_nadine_fmp.controller.AuthenticationController;
import ist.challenge.Alya_nadine_fmp.entity.*;
import ist.challenge.Alya_nadine_fmp.repository.*;
import ist.challenge.Alya_nadine_fmp.response.ResponseHandler;
import ist.challenge.Alya_nadine_fmp.security.JwtUtils;
import ist.challenge.Alya_nadine_fmp.security.MyUserDetails;
import ist.challenge.Alya_nadine_fmp.service.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    private static final String Line = "====================";

    //AUTHENTICATION
    @Override
    public ResponseEntity<Object> authenticationUser(LoginUserRequest userRequest) throws Exception {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userRequest.getUsername(), userRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtUtils.generateJwtToken(authentication);
            MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();

            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());

            JwtResponse jwtResponse = new JwtResponse(jwt, userDetails.getUserId(), userDetails.getUsername(), roles);

            logger.info(Line + "Logger Start Login " + Line);
            logger.info(String.valueOf(jwtResponse));
            logger.info(Line + "Logger End Login " + Line);

            return ResponseHandler.generateResponse("successfully login", HttpStatus.OK, jwtResponse);

        } catch (Exception e) {

            logger.error(Line + " Logger Start Error " + Line);
            logger.error(e.getMessage());
            logger.error(Line + " Logger End Error " + Line);

            return ResponseHandler.generateResponse("Username dan / atau Password kosong", HttpStatus.BAD_REQUEST, "Failed Login!");
        }
    }

    //FIND ALL USER
    @Override
    public ResponseEntity<Object> findAllUser() {
        try {
            List<User> users = userRepository.findAllByOrderByUserIdAsc();
            List<UserResponseDTO> usersDTO = new ArrayList<>();
            logger.info("==================== Logger Start Get All Users ====================");
            for (User user : users) {
                List<Role> roles = roleRepository.findByUsersUserUserId(user.getUserId());
                List<String> role = roles.stream().map(Role::getRoleName).collect(Collectors.toList());
                UserResponseDTO userDTO = user.convertToResponse(role);
                usersDTO.add(userDTO);
                logger.info("-------------------------");
                logger.info("User ID    : " + user.getUserId());
                logger.info("Username   : " + user.getUsername());
                logger.info("Role       : " + role);
            }
            logger.info("==================== Logger End Get All Users ====================");
            logger.info(" ");
            return ResponseHandler.generateResponse("successfully retrieved users", HttpStatus.OK, usersDTO);
        } catch (Exception e) {
            logger.error(Line + " Logger Start Error " + Line);
            logger.error(e.getMessage());
            logger.error(Line + " Logger End Error " + Line);

            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed Get All Users");
        }
    }

    //FIND MY PROFILE
    @Override
    public ResponseEntity<Object> findMyUser(String keyword) throws Exception {
        try {

            List<Role> roles = roleRepository.findByUsersUserUsername(keyword);
            List<String> role = roles.stream().map(Role::getRoleName).collect(Collectors.toList());
            UserResponseDTO result = userRepository.findByUsername(keyword).orElseThrow(() -> new Exception("User not found"))
                    .convertToResponse(role);

            logger.info(Line + "Logger Start findMyProfile " + Line);
            logger.info(String.valueOf(result));
            logger.info(Line + "Logger End findMyProfile " + Line);

            return ResponseHandler.generateResponse("successfully retrieved users", HttpStatus.OK, result);

        } catch (Exception e) {

            logger.error(Line + " Logger Start Error " + Line);
            logger.error(e.getMessage());
            logger.error(Line + " Logger End Error " + Line);

            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed find My Profile");
        }
    }

    //UPDATE/EDIT USER
    @Override
    public ResponseEntity<Object> updateUser(UpdateUserDTO updateUserDTO, Authentication authentication) throws Exception {
        User userApp = this.userRepository.findByUsername(authentication.getName()).orElseThrow(()->new Exception("User not found"));
        try {
            if(updateUserDTO.getUsername()==null||updateUserDTO.getPassword()==null
                    ||updateUserDTO.getRoles()==null){
                throw new Exception("Please check again your input, it can't empty");
            }
            if (userRepository.existsByUsername(updateUserDTO.getUsername())) {
                throw new Exception("Username sudah terpakai");
            }
            if (passwordEncoder.matches(updateUserDTO.getPassword(), userApp.getPassword())){
                throw new Exception("Password tidak boleh sama dengan password sebelumnya");
            }


            this.checkInput(updateUserDTO.getUsername(),
                    updateUserDTO.getPassword());
            User userByUsername = userRepository.findByUsername(authentication.getName()).orElseThrow(()->new Exception("User not found"));
            User user = updateUserDTO.convertToEntity();

            user.setUserId(userByUsername.getUserId());


            List<String> requestRole = updateUserDTO.getRoles();
            this.checkRole(requestRole);
            user.setPassword(passwordEncoder.encode(updateUserDTO.getPassword()));
            User userSave = userRepository.save(user);
            this.addRole(requestRole,userSave);

            UserResponseDTO results = convertResponse(userSave);

            logger.info(Line + "Logger Start Update Profile" + Line);
            logger.info(String.valueOf(results));
            logger.info(Line + "Logger End Update Profile" + Line);

            return ResponseHandler.generateResponse("Success update user!",HttpStatus.CREATED,results);

        }catch (Exception e){

            logger.error(Line + " Logger Start Error " + Line);
            logger.error(e.getMessage());
            logger.error(Line + " Logger End Error " + Line);

            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.BAD_REQUEST,"failed update User");
        }

    }

    //REGISTER
    @Override
    public ResponseEntity<Object> registrationUser(RegistrationDTO registrationDTO) {
        try {
            if(registrationDTO.getUsername()==null||registrationDTO.getPassword()==null){
                throw new Exception("Please check again your input, it can't empty");
            }
            if (userRepository.existsByUsername(registrationDTO.getUsername())) {
                throw new Exception("Username sudah terpakai");
            }

            this.checkInput(registrationDTO.getUsername(),
                    registrationDTO.getPassword());


            User user = registrationDTO.convertToEntity();
            user.setPassword(passwordEncoder.encode(user.getPassword()));

            User userSave = userRepository.save(user);
            this.newRole(userSave);

            UserResponseDTO result = this.convertResponse(userSave);

            logger.info(Line + "Logger Start Registration " + Line);
            logger.info(String.valueOf(result));
            logger.info(Line + "Logger End Registration " + Line);

            return ResponseHandler.generateResponse("successfully registered! please login", HttpStatus.CREATED, result);

        }catch (Exception e){

            logger.error(Line + " Logger Start Error " + Line);
            logger.error(e.getMessage());
            logger.error(Line + " Logger End Error " + Line);

            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.CONFLICT, "Failed create user!");
        }
    }

    public UserResponseDTO convertResponse(User userSave){
        List<Role> rolesUser = roleRepository.findByUsersUserUserId(userSave.getUserId());
        List<String> roleDTO = rolesUser.stream().map(Role::getRoleName).collect(Collectors.toList());
        return userSave.convertToResponse(roleDTO);
    }

    public void newRole(User user) throws Exception {

        Role roleGet = roleRepository.findByRoleNameIgnoreCase("ROLE_USER").orElseThrow(()->new Exception("Role not found"));
        UserRole addRole = new UserRole();

        addRole.setRole(roleGet);
        addRole.setUser(user);

        userRoleRepository.save(addRole);
    }

    public void checkRole(List<String> requestRole){
        requestRole.forEach(role-> {
            try {
                roleRepository.findByRoleNameIgnoreCase(role).orElseThrow(() -> new Exception("Role not found"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void addRole(List<String> requestRole, User user) throws Exception {
        if(requestRole.isEmpty()) {

            Role roleUser = roleRepository.findByRoleNameIgnoreCase("ROLE_USER").orElseThrow(()->new Exception("Role not found"));
            UserRole newRole = new UserRole();
            newRole.setRole(roleUser);
            newRole.setUser(user);
            userRoleRepository.save(newRole);

        } else {

            boolean check = requestRole.stream().anyMatch(role->role.contains("ROLE_ADMIN"));
            if(check){
                throw new Exception("User need admin for ROLE_ADMIN");
            }

            requestRole.forEach(role->{

                try{
                    List<UserRole> userRoles = userRoleRepository.findByRoleRoleNameIgnoreCase(role);
                    boolean checkUser = userRoles.stream().anyMatch(userRole -> Objects.equals(userRole.getUser().getUserId(), user.getUserId()));

                    if(!checkUser){
                        Role roleGet = roleRepository.findByRoleNameIgnoreCase(role).orElseThrow(()->new Exception("Role not found"));
                        UserRole addRole = new UserRole();
                        addRole.setRole(roleGet);
                        addRole.setUser(user);
                        userRoleRepository.save(addRole);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    public void checkInput(String username, String password) throws Exception {

        if (!username.matches("[\\V\\S]{6,20}")){
            throw new Exception("please use the correct username format");
        }

        String passPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,20}$";
        if (!password.matches(passPattern)){
            throw new Exception("please enter the right password format");
        }
    }

}
