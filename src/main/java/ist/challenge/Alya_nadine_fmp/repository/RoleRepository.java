package ist.challenge.Alya_nadine_fmp.repository;

import ist.challenge.Alya_nadine_fmp.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    List<Role> findByUsersUserUserId(Long id);

    List<Role> findByUsersUserUsername(String keyword);

    Optional<Role> findByRoleNameIgnoreCase(String rolename);

}
