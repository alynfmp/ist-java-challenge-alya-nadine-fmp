package ist.challenge.Alya_nadine_fmp.repository;

import ist.challenge.Alya_nadine_fmp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByOrderByUserIdAsc();

    Optional<User> findByUsername(String keyword);

    boolean existsByUsername(String username);

//    boolean matches(String password);
}
