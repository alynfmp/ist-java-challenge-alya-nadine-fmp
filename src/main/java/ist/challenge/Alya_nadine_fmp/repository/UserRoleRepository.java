package ist.challenge.Alya_nadine_fmp.repository;

import ist.challenge.Alya_nadine_fmp.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    List<UserRole> findByRoleRoleNameIgnoreCase(String keyword);

}
