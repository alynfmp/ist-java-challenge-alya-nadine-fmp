package ist.challenge.Alya_nadine_fmp.helper;

public class PasswordNotMatchException extends Throwable {

    public PasswordNotMatchException(String message) {
        super(message);
    }

}
