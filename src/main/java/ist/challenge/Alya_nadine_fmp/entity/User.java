package ist.challenge.Alya_nadine_fmp.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ist.challenge.Alya_nadine_fmp.DTO.UserResponseDTO;
import lombok.*;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "users")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userId")
@Transactional
public class User extends AuditEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(unique = true,nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "user",
            fetch = FetchType.LAZY)
    private List<UserRole> roles;

    public UserResponseDTO convertToResponse(List<String> roles){
        return UserResponseDTO.builder()
                .user_id(this.getUserId())
                .username(this.getUsername())
                .roles(roles)
                .build();
    }
}
