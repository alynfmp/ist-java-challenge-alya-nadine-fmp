package ist.challenge.Alya_nadine_fmp.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import ist.challenge.Alya_nadine_fmp.DTO.LoginUserRequest;
import ist.challenge.Alya_nadine_fmp.DTO.RegistrationDTO;
import ist.challenge.Alya_nadine_fmp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
@Tag(name="00. Authentication")
@SecurityRequirement(name = "bearer-key")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthenticationController {

    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginUserRequest userRequest) throws Exception {
        return userService.authenticationUser(userRequest);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Object> createUser(@RequestBody RegistrationDTO registrationRequest) throws Exception {
        return userService.registrationUser(registrationRequest);
    }

}
