package ist.challenge.Alya_nadine_fmp.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import ist.challenge.Alya_nadine_fmp.DTO.UpdateUserDTO;
import ist.challenge.Alya_nadine_fmp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@Tag(name="01. User")
@SecurityRequirement(name = "bearer-key")
public class UserController {

    private final UserService userService;

    @GetMapping("/dashboard/list-users")
    public ResponseEntity<Object> getAllUser() throws Exception {
        return userService.findAllUser();
    }

    @GetMapping("/user/my-profile")
    public ResponseEntity<?> getMyProfile(Authentication authentication) throws Exception {
        return userService.findMyUser(authentication.getName());
    }

    @PutMapping("/user/edit-user")
    public ResponseEntity<Object> updateUser(@RequestBody UpdateUserDTO user, Authentication authentication) throws Exception {
        return userService.updateUser(user,authentication);
    }


}
